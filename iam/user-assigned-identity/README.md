<!----------------------------------------------------------------------------->

# iam/user-assigned-identity

#### Manage Identity and Access Management (IAM) user assigned identities

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//iam/user-assigned-identity`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_iam_user_assigned_identity" {
  source   = "gitlab.com/bitservices/identity/azure//iam/user-assigned-identity"
  name     = "foobar-identity"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}
```

<!----------------------------------------------------------------------------->
