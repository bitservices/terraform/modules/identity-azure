################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "This user assigned identities unique name within the resource group."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this user assigned identity."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belongs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this user assigned identity."
}

################################################################################
# Resources
################################################################################

resource "azurerm_user_assigned_identity" "object" {
  name                = var.name
  location            = var.location
  resource_group_name = var.group

  tags = {
    "Name"         = var.name
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

################################################################################

output "id" {
  value = azurerm_user_assigned_identity.object.id
}

output "name" {
  value = azurerm_user_assigned_identity.object.name
}

output "group" {
  value = azurerm_user_assigned_identity.object.resource_group_name
}

output "tenant_id" {
  value = azurerm_user_assigned_identity.object.tenant_id
}

output "client_id" {
  value = azurerm_user_assigned_identity.object.client_id
}

output "service_principal_id" {
  value = azurerm_user_assigned_identity.object.principal_id
}

################################################################################
