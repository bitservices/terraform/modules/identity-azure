################################################################################
# Data Sources
################################################################################

data "azurerm_subscription" "current" {}

################################################################################
# Outputs
################################################################################

output "subscription_id" {
  value = data.azurerm_subscription.current.id
}

output "subscription_guid" {
  value = data.azurerm_subscription.current.subscription_id
}

output "subscription_name" {
  value = data.azurerm_subscription.current.display_name
}

output "subscription_state" {
  value = data.azurerm_subscription.current.state
}

output "subscription_quota_id" {
  value = data.azurerm_subscription.current.quota_id
}

output "subscription_tenant_id" {
  value = data.azurerm_subscription.current.tenant_id
}

output "subscription_spending_limit" {
  value = data.azurerm_subscription.current.spending_limit
}

output "subscription_location_placement_id" {
  value = data.azurerm_subscription.current.location_placement_id
}

################################################################################
