################################################################################
# Required Variables
################################################################################

variable "role" {
  type        = string
  description = "The Scoped-ID of the role definition (if custom) or name of the built-in role (if built in)."
}

variable "scope" {
  type        = string
  description = "The scope at which the Role Assignment applies to."
}

variable "principal" {
  type        = string
  description = "The ID of the Principal (User, Group, Service Principal, or Application) to assign the Role Definition to."
}


################################################################################
# Optional Variables
################################################################################

variable "built_in" {
  type        = bool
  default     = false
  description = "Is the role specified a built in role or a custom role."
}

variable "skip_aad_check" {
  type        = bool
  default     = false
  description = "Skip Azure Active Directory checks which may fail due to replication lag. Should only be true for newly provisioned service principals."
}

################################################################################
# Locals
################################################################################

locals {
  role_id   = var.built_in ? null : var.role
  role_name = var.built_in ? var.role : null
}

################################################################################
# Resources
################################################################################

resource "azurerm_role_assignment" "object" {
  scope                            = var.scope
  principal_id                     = var.principal
  role_definition_id               = local.role_id
  role_definition_name             = local.role_name
  skip_service_principal_aad_check = var.skip_aad_check
}

################################################################################
# Outputs
################################################################################

output "built_in" {
  value = var.built_in
}

output "skip_aad_check" {
  value = var.skip_aad_check
}

################################################################################

output "role_id" {
  value = local.role_id
}

output "role_name" {
  value = local.role_name
}

################################################################################

output "id" {
  value = azurerm_role_assignment.object.id
}

output "scope" {
  value = azurerm_role_assignment.object.scope
}

output "principal" {
  value = azurerm_role_assignment.object.principal_id
}

output "principal_type" {
  value = azurerm_role_assignment.object.principal_type
}

################################################################################
