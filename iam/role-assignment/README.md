<!----------------------------------------------------------------------------->

# iam/role-assignment

#### Manage Identity and Access Management (IAM) role assignments

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//iam/role-assignment`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_acr_registry" {
  source   = "gitlab.com/bitservices/container/azure//acr/registry"
  class    = "foobar"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_aad_application" {
  source = "gitlab.com/bitservices/identity/azure//aad/application"
  name   = "foobar-pull"
}

module "my_aad_service_principal" {
  source         = "gitlab.com/bitservices/identity/azure//aad/service-principal"
  application_id = module.my_aad_application.id
}

module "my_iam_role_assignment" {
  source         = "gitlab.com/bitservices/identity/azure//iam/role-assignment"
  role           = "AcrPull"
  scope          = module.my_acr_registry.id
  principal      = module.my_aad_service_principal.id
  built_in       = true
  skip_aad_check = true
}
```

<!----------------------------------------------------------------------------->
