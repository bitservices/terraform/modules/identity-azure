<!----------------------------------------------------------------------------->

# iam/role-assignment

#### Manage Identity and Access Management (IAM) roles

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//iam/role`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_iam_role" {
  source = "gitlab.com/bitservices/identity/azure//iam/role"
  name   = "foobar"

  allow_actions = [
    "Microsoft.Compute/register/action"
  ]
}
```

<!----------------------------------------------------------------------------->
