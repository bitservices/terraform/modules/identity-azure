################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the role."
}

################################################################################
# Optional Variables
################################################################################

variable "scopes" {
  type        = list(string)
  default     = null
  description = "List of scopes that this role can be assigned to."

  validation {
    condition     = try(length(var.scopes) > 0, true)
    error_message = "Do not specify an empty list for scopes. Leave as 'null' to automatically use the current subscription."
  }
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "Description of this role."
}

################################################################################

variable "deny_data" {
  type        = list(string)
  default     = null
  description = "One or more denied data actions."

  validation {
    condition     = try(length(var.deny_data) > 0, true)
    error_message = "Do not specify an empty list for denied data actions. Leave as 'null' if not used."
  }
}

variable "deny_actions" {
  type        = list(string)
  default     = null
  description = "One or more denied actions."

  validation {
    condition     = try(length(var.deny_actions) > 0, true)
    error_message = "Do not specify an empty list for denied actions. Leave as 'null' if not used."
  }
}

################################################################################

variable "allow_data" {
  type        = list(string)
  default     = null
  description = "One or more allowed data actions."

  validation {
    condition     = try(length(var.allow_data) > 0, true)
    error_message = "Do not specify an empty list for allowed data actions. Leave as 'null' if not used."
  }
}

variable "allow_actions" {
  type        = list(string)
  default     = null
  description = "One or more allowed actions."

  validation {
    condition     = try(length(var.allow_actions) > 0, true)
    error_message = "Do not specify an empty list for allowed actions. Leave as 'null' if not used."
  }
}

################################################################################
# Locals
################################################################################

locals {
  scopes = coalesce(var.scopes, tolist([data.azurerm_subscription.current.id]))
}

################################################################################
# Resources
################################################################################

resource "azurerm_role_definition" "object" {
  name              = var.name
  scope             = local.scopes[0]
  description       = var.description
  assignable_scopes = local.scopes

  dynamic "permissions" {
    for_each = (
      var.deny_data != null ||
      var.deny_actions != null ||
      var.allow_data != null ||
      var.allow_actions != null
    ) ? [null] : []

    content {
      actions          = var.allow_actions
      not_actions      = var.deny_actions
      data_actions     = var.allow_data
      not_data_actions = var.deny_data
    }
  }
}

################################################################################
# Outputs
################################################################################

output "scopes" {
  value = local.scopes
}

output "description" {
  value = var.description
}

################################################################################

output "deny_data" {
  value = var.deny_data
}

output "deny_actions" {
  value = var.deny_actions
}

################################################################################

output "allow_data" {
  value = var.allow_data
}

output "allow_actions" {
  value = var.allow_actions
}

################################################################################

output "id" {
  value = azurerm_role_definition.object.role_definition_resource_id
}

output "guid" {
  value = azurerm_role_definition.object.role_definition_id
}

output "name" {
  value = azurerm_role_definition.object.name
}

output "scoped_id" {
  value = azurerm_role_definition.object.id
}

################################################################################
