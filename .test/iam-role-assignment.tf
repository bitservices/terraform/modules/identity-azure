################################################################################
# Modules
################################################################################

module "iam_role_assignment" {
  source         = "../iam/role-assignment"
  role           = module.iam_role.id
  scope          = module.iam_role.scopes[0]
  principal      = module.aad_service_principal.id
  skip_aad_check = true
}

################################################################################
