################################################################################
# Terraform Settings
################################################################################

terraform {
  required_providers {
    azuread = {
      source = "hashicorp/azuread"
    }

    azurerm = {
      source = "hashicorp/azurerm"
    }

    random = {
      source = "hashicorp/random"
    }

    time = {
      source = "hashicorp/time"
    }
  }
}

################################################################################
