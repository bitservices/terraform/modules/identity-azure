################################################################################
# Modules
################################################################################

module "aad_guest" {
  source = "../aad/guest"
  name   = "Foo Bar"
  email  = "foobar@bitservices.io"
}

################################################################################
