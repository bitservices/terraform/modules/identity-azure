################################################################################
# Modules
################################################################################

module "iam_user_assigned_identity" {
  source   = "../iam/user-assigned-identity"
  name     = "foobar-identity"
  group    = local.group
  owner    = local.owner
  company  = local.company
  location = local.location
}

################################################################################
