################################################################################
# Modules
################################################################################

module "aad_application_role_assignment" {
  source               = "../aad/application-role-assignment"
  role                 = module.aad_application_role.value
  principal            = module.aad_user.object
  service_principal_id = module.aad_service_principal.object
}

################################################################################
