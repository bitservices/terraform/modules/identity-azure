<!----------------------------------------------------------------------------->

# identity (azure)

<!----------------------------------------------------------------------------->

## Description

Manage identity resources on [Azure] such as [Azure] Active Directory ([AAD])
and Identity and Access Management (IAM).

<!----------------------------------------------------------------------------->

## Modules

* [aad/application](aad/application/README.md) - Manage [Azure] Active Directory ([AAD]) application registrations.
* [aad/application-role](aad/application-role/README.md) - Manage [Azure] Active Directory ([AAD]) application roles.
* [aad/application-role-assignment](aad/application-role-assignment/README.md) - Manage [Azure] Active Directory ([AAD]) application role assignments.
* [aad/directory/b2c](aad/directory/b2c/README.md) - Manage [Azure] Active Directory ([AAD]) B2C directories.
* [aad/group](aad/group/README.md) - Manage [Azure] Active Directory ([AAD]) groups.
* [aad/guest](aad/guest/README.md) - Manage [Azure] Active Directory ([AAD]) invitations to guest users.
* [aad/service-principal](aad/service-principal/README.md) - Manage [Azure] Active Directory ([AAD]) service principals.
* [aad/user](aad/user/README.md) - Manage [Azure] Active Directory ([AAD]) users.
* [iam/role](iam/role/README.md) - Manage Identity and Access Management (IAM) roles.
* [iam/role-assignment](iam/role-assignment/README.md) - Manage Identity and Access Management (IAM) role assignments.
* [iam/user-assigned-identity](iam/user-assigned-identity/README.md) - Manage Identity and Access Management (IAM) user assigned identities.

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
