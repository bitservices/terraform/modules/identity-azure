################################################################################
# Required Variables
################################################################################

variable "service_principal_id" {
  type        = string
  description = "The service principal client or object ID the application role belongs to."
}

################################################################################
# Optional Variables
################################################################################

variable "service_principal_id_type" {
  type        = string
  default     = "object"
  description = "The ID type for 'service_principal_id'. Must be 'client' or 'object'."

  validation {
    condition     = contains(["client", "object"], var.service_principal_id_type)
    error_message = "The service principal ID type must either be 'client' or 'object'."
  }
}

################################################################################
# Locals
################################################################################

locals {
  service_principal_client_id = var.service_principal_id_type == "client" ? var.service_principal_id : null
  service_principal_object_id = var.service_principal_id_type == "object" ? var.service_principal_id : null
}

################################################################################
# Data Sources
################################################################################

data "azuread_service_principal" "object" {
  client_id = local.service_principal_client_id
  object_id = local.service_principal_object_id
}

################################################################################
# Outputs
################################################################################

output "service_principal_id" {
  value = data.azuread_service_principal.object.id
}

output "service_principal_name" {
  value = data.azuread_service_principal.object.display_name
}

output "service_principal_tags" {
  value = data.azuread_service_principal.object.tags
}

output "service_principal_type" {
  value = data.azuread_service_principal.object.type
}

output "service_principal_notes" {
  value = data.azuread_service_principal.object.notes
}

output "service_principal_client" {
  value = data.azuread_service_principal.object.client_id
}

output "service_principal_object" {
  value = data.azuread_service_principal.object.object_id
}

output "service_principal_tenant" {
  value = data.azuread_service_principal.object.application_tenant_id
}

output "service_principal_enabled" {
  value = data.azuread_service_principal.object.account_enabled
}

output "service_principal_audience" {
  value = data.azuread_service_principal.object.sign_in_audience
}

output "service_principal_features" {
  value = data.azuread_service_principal.object.feature_tags
}

output "service_principal_saml_sso" {
  value = data.azuread_service_principal.object.saml_single_sign_on
}

output "service_principal_saml_url" {
  value = data.azuread_service_principal.object.saml_metadata_url
}

output "service_principal_sso_mode" {
  value = data.azuread_service_principal.object.preferred_single_sign_on_mode
}

output "service_principal_app_roles" {
  value = data.azuread_service_principal.object.app_roles
}

output "service_principal_login_url" {
  value = data.azuread_service_principal.object.login_url
}

output "service_principal_logout_url" {
  value = data.azuread_service_principal.object.logout_url
}

output "service_principal_description" {
  value = data.azuread_service_principal.object.description
}

output "service_principal_app_role_ids" {
  value = data.azuread_service_principal.object.app_role_ids
}

output "service_principal_homepage_url" {
  value = data.azuread_service_principal.object.homepage_url
}

output "service_principal_redirect_urls" {
  value = data.azuread_service_principal.object.redirect_uris
}

output "service_principal_identifier_urls" {
  value = data.azuread_service_principal.object.service_principal_names
}

output "service_principal_alternative_names" {
  value = data.azuread_service_principal.object.alternative_names
}

output "service_principal_app_role_required" {
  value = data.azuread_service_principal.object.app_role_assignment_required
}

output "service_principal_oauth2_permission_scopes" {
  value = data.azuread_service_principal.object.oauth2_permission_scopes
}

output "service_principal_oauth2_permission_scope_ids" {
  value = data.azuread_service_principal.object.oauth2_permission_scope_ids
}

output "service_principal_notification_email_addresses" {
  value = data.azuread_service_principal.object.notification_email_addresses
}

################################################################################
