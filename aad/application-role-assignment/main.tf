################################################################################
# Required Variables
################################################################################

variable "principal" {
  type        = string
  description = "The ID of the Principal (User, Group or Application) to assign the application role to."
}

################################################################################
# Optional Variables
################################################################################

variable "role" {
  type        = string
  default     = null
  description = "The value of the application role to grant permission to."
}

################################################################################
# Locals
################################################################################

locals {
  role_id = var.role == null ? "00000000-0000-0000-0000-000000000000" : data.azuread_service_principal.object.app_role_ids[var.role]
}

################################################################################
# Resources
################################################################################

resource "azuread_app_role_assignment" "object" {
  app_role_id         = local.role_id
  resource_object_id  = data.azuread_service_principal.object.object_id
  principal_object_id = var.principal
}

################################################################################
# Outputs
################################################################################

output "role" {
  value = var.role
}

################################################################################

output "id" {
  value = azuread_app_role_assignment.object.id
}

output "role_id" {
  value = azuread_app_role_assignment.object.app_role_id
}

output "principal" {
  value = azuread_app_role_assignment.object.principal_object_id
}

output "principal_name" {
  value = azuread_app_role_assignment.object.principal_display_name
}

output "principal_type" {
  value = azuread_app_role_assignment.object.principal_type
}

################################################################################
