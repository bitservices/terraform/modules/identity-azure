<!----------------------------------------------------------------------------->

# aad/application-role-assignment

#### Manage [Azure] Active Directory ([AAD]) application role assignments

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/application-role-assignment`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_aad_user" {
  source    = "gitlab.com/bitservices/identity/azure//aad/user"
  lastname  = "Bar"
  firstname = "Foo"
  principal = "foobar@bitservices.io"
}

module "my_aad_application" {
  source = "gitlab.com/bitservices/identity/azure//aad/application"
  name   = "foobar"
}

module "my_aad_service_principal" {
  source         = "gitlab.com/bitservices/identity/azure//aad/service-principal"
  application_id = module.my_aad_application.object
}

module "my_aad_application_role_assignment" {
  source               = "gitlab.com/bitservices/identity/azure//aad/application-role-assignment"
  principal            = module.my_aad_user.object
  service_principal_id = module.my_aad_service_principal.object
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
