<!----------------------------------------------------------------------------->

# aad/group

#### Manage [Azure] Active Directory ([AAD]) groups

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/group`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_aad_group" {
  source = "gitlab.com/bitservices/identity/azure//aad/group"
  name   = "foobar"
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
