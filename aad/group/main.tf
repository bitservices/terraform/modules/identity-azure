################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The display name for the group."
}

################################################################################
# Optional Variables
################################################################################

variable "mail" {
  type        = bool
  default     = false
  description = "Whether the group is a mail enabled, with a shared group mailbox."
}

variable "alias" {
  type        = string
  default     = null
  description = "A mail alias for the group. Uses 'name' by default. Ignored if 'mail' is 'false'."
}

variable "owners" {
  type        = set(string)
  default     = null
  description = "A set of owners. Supported object types are Users, Groups or Service Principals."

  validation {
    condition     = try(length(var.owners) > 0, true)
    error_message = "At least one owner must be specified. To add only the creator as the owner, leave as null."
  }
}

variable "members" {
  type        = set(string)
  default     = null
  description = "A set of members who should be present in this group. Supported object types are Users, Groups or Service Principals. Ignored if 'dynamic_status' is 'true'."

  validation {
    condition     = try(length(var.members) > 0, true)
    error_message = "At least one member must be specified. To add no members, leave as null."
  }
}

variable "unified" {
  type        = bool
  default     = false
  description = "Should the 'Unified' group type be set to create a Microsoft 365 group. Always 'true' if 'mail' is 'true'."
}

variable "duplicates" {
  type        = bool
  default     = false
  description = "Allow creation even if 'name' is a duplicate of an already existing group."
}

variable "description" {
  type        = string
  default     = "Managed By Terraform"
  description = "Description to be assigned to the group."
}

################################################################################

variable "dynamic_rule" {
  type        = string
  default     = null
  description = "The rule that determines membership of this group. Ignored if 'dynamic_status' is 'off'."
}

variable "dynamic_status" {
  type        = string
  default     = "off"
  description = "Dynamic membership status. Must be 'off', 'paused' or 'on'. Setting to 'off' disables dynamic membership. Ignored unless 'dynamic_rule' is set."

  validation {
    condition     = contains(["off", "paused", "on"], var.dynamic_status)
    error_message = "Dynamic membership status must be either 'off', 'paused' or 'on'."
  }
}

################################################################################

variable "security_roles" {
  type        = bool
  default     = false
  description = "Whether the group can be assigned to an Azure Active Directory role. Ignored if 'security_enabled' is 'false'."
}

variable "security_enabled" {
  type        = bool
  default     = true
  description = "Whether the group is a security group for controlling access to in-app resources. Always 'true' if 'mail' is 'false'."
}

################################################################################
# Locals
################################################################################

locals {
  alias            = var.mail ? coalesce(var.alias, var.name) : null
  types            = var.unified || local.dynamic_enabled ? toset(compact([local.dynamic_enabled ? "DynamicMembership" : null, var.unified ? "Unified" : null])) : null
  members          = local.dynamic_status == "off" ? var.members : null
  unified          = var.mail ? true : var.unified
  dynamic_status   = var.dynamic_rule == null ? "off" : var.dynamic_status
  security_roles   = local.security_enabled ? var.security_roles : false
  dynamic_enabled  = local.dynamic_status != "off"
  security_enabled = var.mail ? var.security_enabled : true
}

################################################################################
# Resources
################################################################################

resource "azuread_group" "object" {
  types                   = local.types
  owners                  = var.owners
  members                 = local.members
  description             = var.description
  display_name            = var.name
  mail_enabled            = var.mail
  mail_nickname           = local.alias
  security_enabled        = local.security_enabled
  assignable_to_role      = local.security_roles
  prevent_duplicate_names = var.duplicates ? false : true

  dynamic "dynamic_membership" {
    for_each = local.dynamic_enabled ? [null] : []

    content {
      rule    = var.dynamic_rule
      enabled = local.dynamic_status == "on"
    }
  }
}

################################################################################
# Outputs
################################################################################

output "mail" {
  value = var.mail
}

output "alias" {
  value = local.alias
}

output "owners" {
  value = var.owners
}

output "members" {
  value = local.members
}

output "unified" {
  value = local.unified
}

output "duplicates" {
  value = var.duplicates
}

################################################################################

output "dynamic_rule" {
  value = var.dynamic_rule
}

output "dynamic_status" {
  value = local.dynamic_status
}

output "dynamic_enabled" {
  value = local.dynamic_enabled
}

################################################################################

output "security_roles" {
  value = local.security_roles
}

output "security_enabled" {
  value = local.security_enabled
}

################################################################################

output "types" {
  value = local.types
}

################################################################################

output "id" {
  value = azuread_group.object.id
}

output "name" {
  value = azuread_group.object.display_name
}

output "object" {
  value = azuread_group.object.object_id
}

output "language" {
  value = azuread_group.object.preferred_language
}

output "addresses" {
  value = azuread_group.object.proxy_addresses
}

output "description" {
  value = azuread_group.object.description
}

################################################################################

output "onprem_sam" {
  value = azuread_group.object.onpremises_sam_account_name
}

output "onprem_sid" {
  value = azuread_group.object.onpremises_security_identifier
}

output "onprem_sync" {
  value = azuread_group.object.onpremises_sync_enabled
}

output "onprem_domain" {
  value = azuread_group.object.onpremises_domain_name
}

output "onprem_netbios" {
  value = azuread_group.object.onpremises_netbios_name
}

################################################################################
