################################################################################
# Required Variables
################################################################################

variable "value" {
  type        = string
  description = "The application role value. A unique string value that is used later for looking up this application role."
}

################################################################################
# Optional Variables
################################################################################

variable "name" {
  type        = string
  default     = null
  description = "The display name for the application role."
}

variable "types" {
  type        = set(string)
  description = "A set of values to specify whether this app role definition can be assigned to users and groups (User) and/or other applications (Application)."

  default = [
    "Application",
    "User"
  ]
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "Description of this application role."
}

################################################################################
# Locals
################################################################################

locals {
  name = coalesce(var.name, title(var.value))
}

################################################################################
# Resources
################################################################################

resource "random_uuid" "object" {
  keepers = {
    "name" = var.value
  }
}

################################################################################

resource "azuread_application_app_role" "object" {
  value                = var.value
  role_id              = random_uuid.object.id
  description          = var.description
  display_name         = local.name
  application_id       = data.azuread_application.object.id
  allowed_member_types = var.types
}

################################################################################
# Outputs
################################################################################

output "id" {
  value = azuread_application_app_role.object.role_id
}

output "name" {
  value = azuread_application_app_role.object.display_name
}

output "types" {
  value = azuread_application_app_role.object.allowed_member_types
}

output "value" {
  value = azuread_application_app_role.object.value
}

output "description" {
  value = azuread_application_app_role.object.description
}

################################################################################
