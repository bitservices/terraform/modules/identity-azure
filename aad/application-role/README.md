<!----------------------------------------------------------------------------->

# aad/application-role

#### Manage [Azure] Active Directory ([AAD]) application roles

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/application-role`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_aad_application" {
  source = "gitlab.com/bitservices/identity/azure//aad/application"
  name   = "foobar"
}

module "my_aad_application_role" {
  source         = "gitlab.com/bitservices/identity/azure//aad/application"
  value          = "Admin"
  application_id = module.my_aad_application.object
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
