################################################################################
# Optional Variables
################################################################################

variable "password_attach" {
  type        = bool
  default     = true
  description = "Attach a password for this user. Must be 'true' if creating a member user rather than inviting a guest."
}

################################################################################
# Locals
################################################################################

locals {
  password = var.password_attach ? random_password.object[0].result : null
}

################################################################################
# Resources
################################################################################

resource "random_password" "object" {
  count   = var.password_attach ? 1 : 0
  length  = 64
  special = true

  keepers = {
    "user_principal" = var.name
  }
}

################################################################################
# Outputs
################################################################################

output "password_attach" {
  value = var.password_attach
}

################################################################################

output "password" {
  sensitive = true
  value     = local.password
}

################################################################################
