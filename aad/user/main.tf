################################################################################
# Required Variables
################################################################################

variable "lastname" {
  type        = string
  description = "The user's last name."
}

variable "firstname" {
  type        = string
  description = "The user's first name."
}

variable "principal" {
  type        = string
  description = "The identifier for the user. Normally an email address."
}

################################################################################
# Optional Variables
################################################################################

variable "name" {
  type        = string
  default     = null
  description = "The display name for the user. Uses 'firstname' 'lastname' by default."
}

################################################################################
# Locals
################################################################################

locals {
  name = coalesce(var.name, format("%s %s", var.firstname, var.lastname))
}

################################################################################
# Resources
################################################################################

resource "azuread_user" "object" {
  surname               = var.lastname
  password              = local.password
  given_name            = var.firstname
  display_name          = local.name
  user_principal_name   = var.principal
  force_password_change = true
}

################################################################################
# Outputs
################################################################################

output "lastname" {
  value = var.lastname
}

output "firstname" {
  value = var.firstname
}

output "principal" {
  value = var.principal
}

################################################################################

output "im" {
  value = azuread_user.object.im_addresses
}

output "id" {
  value = azuread_user.object.id
}

output "name" {
  value = azuread_user.object.display_name
}

output "type" {
  value = azuread_user.object.user_type
}

output "object" {
  value = azuread_user.object.object_id
}

output "creation" {
  value = azuread_user.object.creation_type
}

output "addresses" {
  value = azuread_user.object.proxy_addresses
}

output "invitation" {
  value = azuread_user.object.external_user_state
}

################################################################################

output "onprem_dn" {
  value = azuread_user.object.onpremises_distinguished_name
}

output "onprem_sam" {
  value = azuread_user.object.onpremises_sam_account_name
}

output "onprem_sid" {
  value = azuread_user.object.onpremises_security_identifier
}

output "onprem_sync" {
  value = azuread_user.object.onpremises_sync_enabled
}

output "onprem_domain" {
  value = azuread_user.object.onpremises_domain_name
}

output "onprem_principal" {
  value = azuread_user.object.onpremises_user_principal_name
}

################################################################################
