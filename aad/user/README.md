<!----------------------------------------------------------------------------->

# aad/user

#### Manage [Azure] Active Directory ([AAD]) users

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/user`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_aad_user" {
  source    = "gitlab.com/bitservices/identity/azure//aad/user"
  lastname  = "Bar"
  firstname = "Foo"
  principal = "foobar@bitservices.io"
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
