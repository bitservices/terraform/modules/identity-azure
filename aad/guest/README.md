<!----------------------------------------------------------------------------->

# aad/guest

#### Manage [Azure] Active Directory ([AAD]) invitations to guest users

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/guest`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_aad_guest" {
  source = "gitlab.com/bitservices/identity/azure//aad/guest"
  name   = "Foo Bar"
  email  = "foobar@bitservices.io"
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
