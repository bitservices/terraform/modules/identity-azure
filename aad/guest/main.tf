################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the guest to invite to the Active Directory tenant."
}

variable "email" {
  type        = string
  description = "The email address of the guest to invite to the Active Directory tenant."
}

################################################################################
# Optional Variables
################################################################################

variable "type" {
  type        = string
  default     = "Guest"
  description = "The user type of the guest being invited. Must be 'Guest' or 'Member'."

  validation {
    condition     = contains(["Guest", "Member"], var.type)
    error_message = "The user type of the guest being invited must be either 'Guest' or 'Member'."
  }
}

variable "redirect" {
  type        = string
  default     = "https://myaccount.microsoft.com/"
  description = "The URL that the guest should be redirected to once the invitation is redeemed."
}

################################################################################

variable "message_body" {
  type        = string
  default     = null
  description = "Customized message body you want to send if you don't want to send the default message."
}

variable "message_language" {
  type        = string
  default     = "en-GB"
  description = "The language you want to send the default message in. The value specified must be in ISO 639 format. Ignored if 'message_body' is specified."
}

variable "message_extra_recipient" {
  type        = string
  default     = null
  description = "Email addresses of a single additional recipient the invitation message should be sent to."
}

################################################################################
# Locals
################################################################################

locals {
  message_language        = var.message_body != null ? var.message_language : null
  message_extra_recipient = var.message_extra_recipient != null ? tolist([var.message_extra_recipient]) : null
}

################################################################################
# Resources
################################################################################

resource "azuread_invitation" "object" {
  user_type          = var.type
  redirect_url       = var.redirect
  user_display_name  = var.name
  user_email_address = var.email

  message {
    body                  = var.message_body
    language              = local.message_language
    additional_recipients = local.message_extra_recipient
  }
}

################################################################################
# Outputs
################################################################################

output "type" {
  value = var.type
}

output "redirect" {
  value = var.redirect
}

################################################################################

output "message_body" {
  value = var.message_body
}

output "message_language" {
  value = local.message_language
}

output "message_extra_recipient" {
  value = var.message_extra_recipient
}

################################################################################

output "id" {
  value = azuread_invitation.object.id
}

output "name" {
  value = azuread_invitation.object.user_display_name
}

output "email" {
  value = azuread_invitation.object.user_email_address
}

output "object" {
  value = azuread_invitation.object.user_id
}

output "redeem" {
  value = azuread_invitation.object.redeem_url
}

################################################################################
