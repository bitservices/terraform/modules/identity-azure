<!----------------------------------------------------------------------------->

# aad/directory/b2c

#### Manage [Azure] Active Directory ([AAD]) B2C directories

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/directory/b2c`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_aad_directory_b2c" {
  source  = "gitlab.com/bitservices/identity/azure//aad/directory/b2c"
  name    = "foobar-b2c"
  group   = module.my_resource_group.id
  owner   = var.owner
  company = var.company
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
