################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the Azure Active Directory B2C tenant."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

################################################################################
# Optional Variables
################################################################################

variable "sku" {
  type        = string
  default     = "PremiumP1"
  description = "Billing SKU for the B2C tenant. Must be 'PremiumP1' or 'PremiumP2'."
}

variable "domain" {
  type        = string
  default     = null
  description = "Domain name of the B2C tenant, including the '.onmicrosoft.com' suffix. Defaults to using 'name' as a prefix."
}

variable "region" {
  type        = string
  default     = "Europe"
  description = "Region in which the B2C tenant is hosted and data resides. Must be 'United States', 'Europe', 'Asia Pacific' or 'Australia'."
}

variable "country" {
  type        = string
  default     = "GB"
  description = "Country code of the B2C tenant. See: https://learn.microsoft.com/en-gb/azure/active-directory-b2c/data-residency"
}

################################################################################
# Locals
################################################################################

locals {
  domain = coalesce(var.domain, format("%s.onmicrosoft.com", replace(var.name, "/[_-]/", "")))
}

################################################################################
# Resources
################################################################################

resource "azurerm_aadb2c_directory" "object" {
  sku_name                = var.sku
  domain_name             = local.domain
  country_code            = var.country
  display_name            = var.name
  resource_group_name     = var.group
  data_residency_location = var.region

  tags = {
    "SKU"          = var.sku
    "Name"         = var.name
    "Group"        = var.group
    "Owner"        = var.owner
    "Domain"       = local.domain
    "Region"       = var.region
    "Company"      = var.company
    "Country"      = var.country
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "Company" {
  value = var.company
}

################################################################################

output "sku" {
  value = var.sku
}

output "region" {
  value = var.region
}

output "country" {
  value = var.country
}

################################################################################

output "id" {
  value = azurerm_aadb2c_directory.object.id
}

output "name" {
  value = azurerm_aadb2c_directory.object.display_name
}

output "group" {
  value = azurerm_aadb2c_directory.object.resource_group_name
}

output "domain" {
  value = azurerm_aadb2c_directory.object.domain_name
}

output "tenant" {
  value = azurerm_aadb2c_directory.object.tenant_id
}

output "billing_type" {
  value = azurerm_aadb2c_directory.object.billing_type
}

output "billing_start" {
  value = azurerm_aadb2c_directory.object.effective_start_date
}

################################################################################
