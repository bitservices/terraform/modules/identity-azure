################################################################################
# Data Sources
################################################################################

data "azuread_client_config" "current" {}

################################################################################
# Outputs
################################################################################

output "client_config_id" {
  value = data.azuread_client_config.current.object_id
}

output "client_config_client_id" {
  value = data.azuread_client_config.current.client_id
}

output "client_config_tenant_id" {
  value = data.azuread_client_config.current.tenant_id
}

################################################################################
