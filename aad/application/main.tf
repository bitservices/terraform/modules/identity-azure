################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The display name for the application."
}

################################################################################
# Optional Variables
################################################################################

variable "type" {
  type        = string
  default     = "web"
  description = "Sets the application type. Must be either 'native', 'spa' or 'web'."

  validation {
    condition     = contains(["native", "spa", "web"], var.type)
    error_message = "The application type must either be 'native' or 'web'."
  }
}

variable "image" {
  type        = string
  default     = null
  description = "Path to a PNG image to use as the applications logo."
}

variable "owners" {
  type        = list(string)
  default     = null
  description = "A list of object IDs that will be set as owners for this application. If unspecified the current context is used."
}

variable "audience" {
  type        = string
  default     = "AzureADMyOrg"
  description = "The Microsoft account types that are supported for the current application. Must be one of 'AzureADMyOrg', 'AzureADMultipleOrgs', 'AzureADandPersonalMicrosoftAccount' or 'PersonalMicrosoftAccount'."
}

variable "terms_url" {
  type        = string
  default     = null
  description = "URL of the application's terms of service statement."
}

variable "reply_urls" {
  type        = list(string)
  default     = null
  description = "A list of URLs that user tokens are sent to for sign in, or the redirect URLs that OAuth 2.0 authorization codes and access tokens are sent to."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "A description of the application, as shown to end users."
}

variable "group_claims" {
  type        = set(string)
  default     = ["SecurityGroup"]
  description = "Configures the groups claim issued in a user or OAuth 2.0 access token that the app expects. Possible values are 'None', 'SecurityGroup', 'DirectoryRole', 'ApplicationGroup' or 'All'."
}

variable "identifier_urls" {
  type        = list(string)
  default     = null
  description = "A list of user-defined URLs that uniquely identify an application within it's Azure AD tenant, or within a verified custom domain if the application is multi-tenant."
}

variable "prevent_duplicates" {
  type        = bool
  default     = true
  description = "If 'true', will return an error if an existing application is found with the same name."
}

variable "privacy_statement_url" {
  type        = string
  default     = null
  description = "URL of the application's privacy statement."
}

################################################################################

variable "api_permissions" {
  default     = []
  description = "API permissions exposed by this application."

  type = list(object({
    known_applications    = optional(set(string))
    mapped_claims_enabled = optional(bool, false)
    token_version         = optional(number, 2)

    oauth2_permissions = list(object({
      admin_consent_description  = optional(string)
      admin_consent_display_name = optional(string)
      enabled                    = optional(bool, true)
      type                       = optional(string, "User")
      user_consent_description   = optional(string)
      user_consent_display_name  = optional(string)
      value                      = string
    }))
  }))
}

################################################################################

variable "feature_hide" {
  type        = bool
  default     = false
  description = "Whether this app is invisible to users in My Apps and Office 365 Launcher."
}

variable "feature_gallery" {
  type        = bool
  default     = false
  description = "Whether this application represents a gallery application for linked service principals."
}

variable "feature_custom_sso" {
  type        = bool
  default     = false
  description = "Whether this application represents a custom SAML application for linked service principals."
}

variable "feature_enterprise" {
  type        = bool
  default     = false
  description = "Whether this application represents an Enterprise Application for linked service principals."
}

################################################################################

variable "resource_access_list" {
  default     = []
  description = "A structured set of access requirements for this application resource."

  type = list(object({
    resource_application_id = string
    resource_application_access = list(object({
      id   = string
      type = string
    }))
  }))
}

################################################################################

variable "web_logout_url" {
  type        = string
  default     = null
  description = "The URL to redirect to after logging out from the web application. Ignored unless 'type' is 'web'."
}

variable "web_homepage_url" {
  type        = string
  default     = null
  description = "The URL to the web application's home page. Ignored unless 'type' is 'web'."
}

variable "web_id_token_enabled" {
  type        = bool
  default     = false
  description = "Whether this web application can request an ID token using OAuth 2.0 implicit flow. Ignored unless 'type' is 'web'."
}

variable "web_access_token_enabled" {
  type        = bool
  default     = false
  description = "Whether this web application can request an access token using OAuth 2.0 implicit flow. Ignored unless 'type' is 'web'."
}

################################################################################
# Locals
################################################################################

locals {
  image  = var.image == null ? null : filebase64(var.image)
  owners = var.owners == null ? tolist([data.azuread_client_config.current.object_id]) : var.owners
}

################################################################################
# Resources
################################################################################

resource "random_uuid" "object" {
  keepers = {
    "name" = var.name
  }
}

################################################################################

resource "azuread_application" "object" {
  owners                         = local.owners
  logo_image                     = local.image
  description                    = var.description
  display_name                   = var.name
  identifier_uris                = var.identifier_urls
  sign_in_audience               = var.audience
  terms_of_service_url           = var.terms_url
  privacy_statement_url          = var.privacy_statement_url
  group_membership_claims        = var.group_claims
  prevent_duplicate_names        = var.prevent_duplicates
  fallback_public_client_enabled = var.type == "native"

  dynamic "api" {
    for_each = var.api_permissions

    content {
      known_client_applications      = length(api.value.known_applications) == 0 ? null : api.value.known_applications
      mapped_claims_enabled          = api.value.mapped_claims_enabled
      requested_access_token_version = api.value.token_version

      dynamic "oauth2_permission_scope" {
        for_each = api.value.oauth2_permissions

        content {
          admin_consent_description  = coalesce(oauth2_permission_scope.value.admin_consent_description, format("Allow the application to access %s on behalf of the signed-in user.", var.name))
          admin_consent_display_name = coalesce(oauth2_permission_scope.value.admin_consent_display_name, format("Access %s", var.name))
          enabled                    = oauth2_permission_scope.value.enabled
          id                         = uuidv5(random_uuid.object.result, format("api/oauth2-permission-scope/%s/%s", oauth2_permission_scope.value.type, oauth2_permission_scope.value.value))
          type                       = oauth2_permission_scope.value.type
          user_consent_description   = coalesce(oauth2_permission_scope.value.user_consent_description, format("Allow the application to access %s on your behalf.", var.name))
          user_consent_display_name  = coalesce(oauth2_permission_scope.value.user_consent_display_name, format("Access %s", var.name))
          value                      = oauth2_permission_scope.value.value
        }
      }
    }
  }

  feature_tags {
    hide                  = var.feature_hide
    gallery               = var.feature_gallery
    enterprise            = var.feature_enterprise
    custom_single_sign_on = var.feature_custom_sso
  }

  dynamic "public_client" {
    for_each = var.type == "native" ? tolist([var.type]) : []

    content {
      redirect_uris = var.reply_urls
    }
  }

  dynamic "required_resource_access" {
    for_each = var.resource_access_list

    content {
      resource_app_id = required_resource_access.value.resource_application_id

      dynamic "resource_access" {
        for_each = required_resource_access.value.resource_application_access

        content {
          id   = resource_access.value.id
          type = resource_access.value.type
        }
      }
    }
  }

  dynamic "single_page_application" {
    for_each = var.type == "spa" ? tolist([var.type]) : []

    content {
      redirect_uris = var.reply_urls
    }
  }

  dynamic "web" {
    for_each = var.type == "web" ? tolist([var.type]) : []

    content {
      logout_url    = var.web_logout_url
      homepage_url  = var.web_homepage_url
      redirect_uris = var.reply_urls

      implicit_grant {
        id_token_issuance_enabled     = var.web_id_token_enabled
        access_token_issuance_enabled = var.web_access_token_enabled
      }
    }
  }

  lifecycle {
    ignore_changes = [
      app_role
    ]
  }
}

################################################################################
# Outputs
################################################################################

output "type" {
  value = var.type
}

output "owners" {
  value = local.owners
}

output "audience" {
  value = var.audience
}

output "terms_url" {
  value = var.terms_url
}

output "reply_urls" {
  value = var.reply_urls
}

output "group_claims" {
  value = var.group_claims
}

output "identifier_urls" {
  value = var.identifier_urls
}

output "prevent_duplicates" {
  value = var.prevent_duplicates
}

output "privacy_statement_url" {
  value = var.privacy_statement_url
}

################################################################################

output "api_permissions" {
  value = var.api_permissions
}

################################################################################

output "feature_hide" {
  value = var.feature_hide
}

output "feature_gallery" {
  value = var.feature_gallery
}

output "feature_custom_sso" {
  value = var.feature_custom_sso
}

output "feature_enterprise" {
  value = var.feature_enterprise
}

################################################################################

output "resource_access_list" {
  value = var.resource_access_list
}

################################################################################

output "web_logout_url" {
  value = var.web_logout_url
}

output "web_homepage_url" {
  value = var.web_homepage_url
}

output "web_id_token_enabled" {
  value = var.web_id_token_enabled
}

output "web_access_token_enabled" {
  value = var.web_access_token_enabled
}

################################################################################

output "id" {
  value = azuread_application.object.id
}

output "name" {
  value = azuread_application.object.display_name
}

output "image" {
  value = azuread_application.object.logo_url
}

output "client" {
  value = azuread_application.object.client_id
}

output "object" {
  value = azuread_application.object.object_id
}

output "disabled" {
  value = azuread_application.object.disabled_by_microsoft
}

output "publisher" {
  value = azuread_application.object.publisher_domain
}

output "description" {
  value = azuread_application.object.description
}

output "app_role_ids" {
  value = azuread_application.object.app_role_ids
}

output "oauth2_permissions" {
  value = azuread_application.object.oauth2_permission_scope_ids
}

################################################################################

output "uuid_namespace" {
  value = random_uuid.object.result
}

################################################################################
