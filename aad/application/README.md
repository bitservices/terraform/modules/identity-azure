<!----------------------------------------------------------------------------->

# aad/application

#### Manage [Azure] Active Directory ([AAD]) application registrations

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/application`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_aad_application" {
  source = "gitlab.com/bitservices/identity/azure//aad/application"
  name   = "foobar"
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
