################################################################################
# Required Variables
################################################################################

variable "application_id" {
  type        = string
  description = "The application client or object ID this service principal is for."
}

################################################################################
# Optional Variables
################################################################################

variable "application_id_type" {
  type        = string
  default     = "object"
  description = "The ID type for 'application_id'. Must be 'client' or 'object'."

  validation {
    condition     = contains(["client", "object"], var.application_id_type)
    error_message = "The application ID type must either be 'client' or 'object'."
  }
}

################################################################################
# Locals
################################################################################

locals {
  application_client_id = var.application_id_type == "client" ? var.application_id : null
  application_object_id = var.application_id_type == "object" ? var.application_id : null
}

################################################################################
# Data Sources
################################################################################

data "azuread_application" "object" {
  client_id = local.application_client_id
  object_id = local.application_object_id
}

################################################################################
# Outputs
################################################################################

output "application_id" {
  value = data.azuread_application.object.id
}

output "application_api" {
  value = data.azuread_application.object.api
}

output "application_web" {
  value = data.azuread_application.object.web
}

output "application_name" {
  value = data.azuread_application.object.display_name
}

output "application_tags" {
  value = data.azuread_application.object.tags
}

output "application_image" {
  value = data.azuread_application.object.logo_url
}

output "application_client" {
  value = data.azuread_application.object.client_id
}

output "application_object" {
  value = data.azuread_application.object.object_id
}

output "application_owners" {
  value = data.azuread_application.object.owners
}

output "application_audience" {
  value = data.azuread_application.object.sign_in_audience
}

output "application_disabled" {
  value = data.azuread_application.object.disabled_by_microsoft
}

output "application_features" {
  value = data.azuread_application.object.feature_tags
}

output "application_app_roles" {
  value = data.azuread_application.object.app_roles
}

output "application_publisher" {
  value = data.azuread_application.object.publisher_domain
}

output "aplication_description" {
  value = data.azuread_application.object.description
}

output "application_terms_url" {
  value = data.azuread_application.object.terms_of_service_url
}

output "application_support_url" {
  value = data.azuread_application.object.support_url
}

output "application_app_role_ids" {
  value = data.azuread_application.object.app_role_ids
}

output "application_group_claims" {
  value = data.azuread_application.object.group_membership_claims
}

output "application_marketing_url" {
  value = data.azuread_application.object.marketing_url
}

output "application_public_client" {
  value = data.azuread_application.object.public_client
}

output "application_identifier_urls" {
  value = data.azuread_application.object.identifier_uris
}

output "application_optional_claims" {
  value = data.azuread_application.object.optional_claims
}

output "application_device_only_auth" {
  value = data.azuread_application.object.device_only_auth_enabled
}

output "application_resource_access_list" {
  value = data.azuread_application.object.required_resource_access
}

output "application_privacy_statement_url" {
  value = data.azuread_application.object.privacy_statement_url
}

output "application_single_page_application" {
  value = data.azuread_application.object.single_page_application
}

output "application_oauth2_permission_scope_ids" {
  value = data.azuread_application.object.oauth2_permission_scope_ids
}

output "application_oauth2_post_response_required" {
  value = data.azuread_application.object.oauth2_post_response_required
}

output "application_fallback_public_client_enabled" {
  value = data.azuread_application.object.fallback_public_client_enabled
}

################################################################################
