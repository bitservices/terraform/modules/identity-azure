################################################################################
# Optional Variables
################################################################################

variable "notes" {
  type        = string
  default     = null
  description = "A free text field to capture information about the service principal, typically used for operational purposes."
}

variable "owners" {
  type        = list(string)
  default     = null
  description = "A list of object IDs that will be set as owners for this service principal. If unspecified the linked application owners are propogated."
}

variable "enabled" {
  type        = bool
  default     = true
  description = "Whether or not the service principal account is enabled."
}

variable "existing" {
  type        = bool
  default     = false
  description = "When 'true', any existing service principal linked to the same application will be automatically imported. Not recommended."
}

variable "sso_mode" {
  type        = string
  default     = null
  description = "The single sign-on mode configured for this service principal. Azure AD uses the preferred single sign-on mode to launch the service principal from Microsoft 365 or the Azure AD My Apps. If specified, supported values are 'oidc', 'password', 'saml' or 'notSupported'."
}

variable "login_url" {
  type        = string
  default     = null
  description = "The URL where the service provider redirects the user to Azure AD to authenticate. Azure AD uses the URL to launch the linked application from Microsoft 365 or the Azure AD My Apps."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "A description of the service principal provided for internal end-users."
}

variable "alternative_names" {
  type        = set(string)
  default     = null
  description = "A set of alternative names for the service principal account."
}

variable "notification_addresses" {
  type        = set(string)
  default     = null
  description = "A set of email addresses where Azure AD sends a notification when the active certificate is near the expiration date. This is only for the certificates used to sign the SAML token issued for Azure AD Gallery applications."
}

variable "app_role_assignment_required" {
  type        = bool
  default     = false
  description = "Does this Service Principal require an AppRoleAssignment to a user or group before Azure AD will issue a user or access token to the linked application?"
}

################################################################################

variable "feature_hide" {
  type        = bool
  default     = false
  description = "Whether this service principal represents an application that is invisible to users in My Apps and Office 365 Launcher. If it is already enabled for the linked application, it will be propogated automatically."
}

variable "feature_gallery" {
  type        = bool
  default     = false
  description = "Whether this service principal represents a gallery application. If it is already enabled for the linked application, it will be propogated automatically."
}

variable "feature_custom_sso" {
  type        = bool
  default     = false
  description = "Whether this service principal represents a custom SAML application. If it is already enabled for the linked application, it will be propogated automatically."
}

variable "feature_enterprise" {
  type        = bool
  default     = false
  description = "Whether this service principal represents an Enterprise Application. If it is already enabled for the linked application, it will be propogated automatically."
}

################################################################################
# Locals
################################################################################

locals {
  owners             = coalesce(var.owners, data.azuread_application.object.owners)
  feature_hide       = try(merge(data.azuread_application.object.feature_tags).hide, false) || var.feature_hide
  feature_gallery    = try(merge(data.azuread_application.object.feature_tags).gallery, false) || var.feature_gallery
  feature_custom_sso = try(merge(data.azuread_application.object.feature_tags).enterprise, false) || var.feature_custom_sso
  feature_enterprise = try(merge(data.azuread_application.object.feature_tags).custom_single_sign_on, false) || var.feature_enterprise
}

################################################################################
# Resources
################################################################################

resource "azuread_service_principal" "object" {
  notes                         = var.notes
  owners                        = local.owners
  client_id                     = data.azuread_application.object.client_id
  login_url                     = var.login_url
  description                   = var.description
  use_existing                  = var.existing
  account_enabled               = var.enabled
  alternative_names             = var.alternative_names
  app_role_assignment_required  = var.app_role_assignment_required
  notification_email_addresses  = var.notification_addresses
  preferred_single_sign_on_mode = var.sso_mode

  feature_tags {
    hide                  = local.feature_hide
    gallery               = local.feature_gallery
    enterprise            = local.feature_enterprise
    custom_single_sign_on = local.feature_custom_sso
  }
}

################################################################################
# Outputs
################################################################################

output "notes" {
  value = var.notes
}

output "owners" {
  value = local.owners
}

output "enabled" {
  value = var.enabled
}

output "existing" {
  value = var.existing
}

output "sso_mode" {
  value = var.sso_mode
}

output "login_url" {
  value = var.login_url
}

output "description" {
  value = var.description
}

output "alternative_names" {
  value = var.alternative_names
}

output "notification_addresses" {
  value = var.notification_addresses
}

################################################################################

output "feature_hide" {
  value = local.feature_hide
}

output "feature_gallery" {
  value = local.feature_gallery
}

output "feature_custom_sso" {
  value = local.feature_custom_sso
}

output "feature_enterprise" {
  value = local.feature_enterprise
}

################################################################################

output "id" {
  value = azuread_service_principal.object.id
}

output "name" {
  value = azuread_service_principal.object.display_name
}

output "type" {
  value = azuread_service_principal.object.type
}

output "client" {
  value = azuread_service_principal.object.client_id
}

output "object" {
  value = azuread_service_principal.object.object_id
}

output "tenant" {
  value = azuread_service_principal.object.application_tenant_id
}

output "audience" {
  value = azuread_service_principal.object.sign_in_audience
}

output "app_roles" {
  value = azuread_service_principal.object.app_roles
}

output "logout_url" {
  value = azuread_service_principal.object.logout_url
}

output "app_role_ids" {
  value = azuread_service_principal.object.app_role_ids
}

output "homepage_url" {
  value = azuread_service_principal.object.homepage_url
}

output "redirect_urls" {
  value = azuread_service_principal.object.redirect_uris
}

output "identifier_urls" {
  value = azuread_service_principal.object.service_principal_names
}

output "saml_metadata_url" {
  value = azuread_service_principal.object.saml_metadata_url
}

output "oauth2_permission_scopes" {
  value = azuread_service_principal.object.oauth2_permission_scopes
}

output "oauth2_permission_scope_ids" {
  value = azuread_service_principal.object.oauth2_permission_scope_ids
}

output "app_role_assignment_required" {
  value = azuread_service_principal.object.app_role_assignment_required
}

################################################################################
