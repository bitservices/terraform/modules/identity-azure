<!----------------------------------------------------------------------------->

# aad/service-principal

#### Manage [Azure] Active Directory ([AAD]) service principals

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/azure//aad/service-principal`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_aad_application" {
  source = "gitlab.com/bitservices/identity/azure//aad/application"
  name   = "foobar"
}

module "my_aad_service_principal" {
  source         = "gitlab.com/bitservices/identity/azure//aad/service-principal"
  application_id = module.my_aad_application.object
}
```

<!----------------------------------------------------------------------------->

[AAD]:   https://azure.microsoft.com/services/active-directory/
[Azure]: https://azure.microsoft.com/

<!----------------------------------------------------------------------------->
