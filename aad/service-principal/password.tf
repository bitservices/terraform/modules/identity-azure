################################################################################
# Optional Variables
################################################################################

variable "password_name" {
  type        = string
  default     = "Managed by Terraform"
  description = "A name for the password attached to the service principal."
}

variable "password_enabled" {
  type        = bool
  default     = false
  description = "Should this service principal have a password generated for it?"
}

################################################################################

variable "password_expires" {
  type        = bool
  default     = true
  description = "Should the password expire? If 'false' sets 'password_expires_absolute' to '2059-12-31T23:59:59Z'."
}

variable "password_expires_days" {
  type        = number
  default     = 365
  description = "How many days the password will be valid for from time of creation. Ignored if 'password_expires_absolute' is set."
}

variable "password_expires_hours" {
  type        = number
  default     = null
  description = "How many hours the password will be valid for from time of creation. Overrides 'password_expires_days'. Ignored if 'password_expires_absolute' is set."
}

variable "password_expires_absolute" {
  type        = string
  default     = null
  description = "The end date which the password is valid until, formatted as a RFC3339 date string."
}

################################################################################

variable "password_rotate_days" {
  type        = number
  default     = null
  description = "After how many days should the password be rotated. Overrides 'password_rotate_percent'. Ignored if 'password_expires_absolute' is set."
}

variable "password_rotate_hours" {
  type        = number
  default     = null
  description = "After how many days should the password be rotated. Overrides 'password_rotate_days' and 'password_rotate_percent'. Ignored if 'password_expires_absolute' is set."
}

variable "password_rotate_percent" {
  type        = number
  default     = 50
  description = "At what point, with 0% being now and 100% being at expiry, should the password be rotated? Ignored if 'password_expires_absolute' is set."
}

################################################################################

variable "password_start_absolute" {
  type        = string
  default     = null
  description = "The start date which the password is valid from, formatted as a RFC3339 date string"
}

################################################################################
# Locals
################################################################################

locals {
  password_rotate           = local.password_expires_override == null
  password_rotate_hours     = local.password_rotate ? coalesce(var.password_rotate_hours, try(var.password_rotate_days * 24, null), ceil(local.password_expires_hours * var.password_rotate_percent / 100)) : null
  password_expires_hours    = coalesce(var.password_expires_hours, var.password_expires_days * 24)
  password_start_absolute   = coalesce(var.password_start_absolute, plantimestamp())
  password_expires_absolute = coalesce(local.password_expires_override, timeadd(local.password_start_absolute, local.password_expires_relative))
  password_expires_override = var.password_expires ? var.password_expires_absolute : "2059-12-31T23:59:59Z"
  password_expires_relative = format("%dh", local.password_expires_hours)
}

################################################################################
# Resources
################################################################################

resource "time_static" "object" {
  count   = var.password_enabled ? 1 : 0
  rfc3339 = local.password_expires_absolute

  triggers = {
    "absolute" = local.password_expires_override
    "relative" = local.password_expires_relative
  }

  lifecycle {
    ignore_changes = [
      rfc3339
    ]
  }
}

################################################################################

resource "time_rotating" "object" {
  count          = var.password_enabled && local.password_rotate ? 1 : 0
  rotation_hours = local.password_rotate_hours
}

################################################################################

resource "azuread_service_principal_password" "object" {
  count                = var.password_enabled ? 1 : 0
  end_date             = time_static.object[0].rfc3339
  start_date           = var.password_start_absolute
  display_name         = var.password_name
  service_principal_id = azuread_service_principal.object.id

  rotate_when_changed = {
    "rotation" = length(time_rotating.object) == 1 ? time_rotating.object[0].id : null
  }
}

################################################################################
# Outputs
################################################################################

output "password_enabled" {
  value = var.password_enabled
}

################################################################################

output "password_expires" {
  value = var.password_expires
}

################################################################################

output "password_rotate" {
  value = local.password_rotate
}

output "password_rotate_hours" {
  value = local.password_rotate_hours
}

################################################################################

output "password_id" {
  value = length(azuread_service_principal_password.object) == 1 ? azuread_service_principal_password.object[0].key_id : null
}

output "password_name" {
  value = length(azuread_service_principal_password.object) == 1 ? azuread_service_principal_password.object[0].display_name : null
}

output "password_value" {
  sensitive = true
  value     = length(azuread_service_principal_password.object) == 1 ? azuread_service_principal_password.object[0].value : null
}

output "password_start_absolute" {
  value = length(azuread_service_principal_password.object) == 1 ? azuread_service_principal_password.object[0].start_date : null
}

output "password_expires_absolute" {
  value = length(azuread_service_principal_password.object) == 1 ? azuread_service_principal_password.object[0].end_date : null
}

################################################################################
